#!/usr/bin/env python

import rospy
import sys
from math import pi
import numpy

import actionlib
import moveit_commander
import tf2_ros
import geometry_msgs
import tf2_geometry_msgs
import moveit_msgs
from omtp_gazebo.msg import LogicalCameraImage
from omtp_gazebo.srv import VacuumGripperControl
import tf2_ros
import tf2_geometry_msgs
import geometry_msgs
from math import pi
from tf.transformations import quaternion_from_euler


def logical_camera2_callback(data):
	# global object_world_pose
	# Check if the logical camera has seen our box which has the name 'object'.
	if (data.models[-1].type == 'object'):
		# Create a pose stamped message type from the camera image topic.
		object_pose = geometry_msgs.msg.PoseStamped()
		object_pose.header.stamp = rospy.Time.now()
		object_pose.header.frame_id = "logical_camera_2_frame"
		object_pose.pose.position.x = data.models[-1].pose.position.x
		object_pose.pose.position.y = data.models[-1].pose.position.y
		object_pose.pose.position.z = data.models[-1].pose.position.z
		object_pose.pose.orientation.x = data.models[-1].pose.orientation.x
		object_pose.pose.orientation.y = data.models[-1].pose.orientation.y
		object_pose.pose.orientation.z = data.models[-1].pose.orientation.z
		object_pose.pose.orientation.w = data.models[-1].pose.orientation.w
		while True:
			try:
				object_world_pose = tf_buffer.transform(object_pose, "world")
				return object_world_pose
			except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
				print(e)
				continue
	else:
		# Do nothing.
		print('object not detected')

def lecture6_assignment4():
	robot2_group.set_named_target("R2Home")
	robot2_group.go()

	# Print message
	rospy.loginfo('Go to Pre Grasp')

	message = rospy.wait_for_message('/omtp/logical_camera_2', LogicalCameraImage)
	object_world_pose = logical_camera2_callback(message)

	print "Going to object pose"
	link6_rotation = quaternion_from_euler(pi, 0, pi/2)
	object_world_pose.pose.orientation.x, object_world_pose.pose.orientation.y, object_world_pose.pose.orientation.z, object_world_pose.pose.orientation.w = link6_rotation
	object_world_pose.pose.position.z += -0.04
	robot2_group.set_pose_target(object_world_pose)
	robot2_group.go()
	rospy.loginfo('Gripping')
	gripper2(True)
	object_world_pose.pose.position.z += 0.2
	robot2_group.set_pose_target(object_world_pose)
	robot2_group.go()
	rospy.loginfo('Pick up')

	robot2_group.set_named_target("R2PreGrasp")
	robot2_group.go()
	robot2_group.set_named_target("R2Place")
	robot2_group.go()
	rospy.loginfo('Placing')
	gripper2(False)


if __name__ == '__main__':
	moveit_commander.roscpp_initialize(sys.argv)
	rospy.init_node('lecture6_assignment4',anonymous=True)
	robot2_group = moveit_commander.MoveGroupCommander("robot2")

	robot2_client = actionlib.SimpleActionClient('execute_trajectory',moveit_msgs.msg.ExecuteTrajectoryAction)
	robot2_client.wait_for_server()
	rospy.loginfo('Execute Trajectory server is available for robot2')
	# Create a TF buffer in the global scope
	tf_buffer = tf2_ros.Buffer()
	tf_listener = tf2_ros.TransformListener(tf_buffer)

	gripper2 = rospy.ServiceProxy('gripper2/control',VacuumGripperControl)
	lecture6_assignment4()