# Lecture 6 Assignment:
Set up and configure two logical cameras in the world and verify with the tf three that they are present.
![Figure of the tf three](frames.png).
![Figure of the the workspace](asgn2.png).
Implement an object and complete the lecture6_assignement3 script, which will then transform the pose of the object to the suction_cup for robot2.

In the fourth assignment a python script for the second robot has been made that pick up an object next to the robot and place it in to the bin. Our solution can be seen in the gif.
![UR robot Moving](lecture6.gif)

