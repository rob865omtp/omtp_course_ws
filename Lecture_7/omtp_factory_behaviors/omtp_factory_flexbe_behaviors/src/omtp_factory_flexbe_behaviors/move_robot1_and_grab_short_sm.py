#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from omtp_factory_flexbe_states.set_conveyor_power_state import SetConveyorPowerState
from flexbe_manipulation_states.moveit_to_joints_dyn_state import MoveitToJointsDynState as flexbe_manipulation_states__MoveitToJointsDynState
from omtp_factory_flexbe_states.vacuum_gripper_control_state import VacuumGripperControlState
from omtp_factory_flexbe_states.compute_grasp_state import ComputeGraspState
from omtp_factory_flexbe_states.control_feeder_state import ControlFeederState
from omtp_factory_flexbe_states.detect_part_camera_state import DetectPartCameraState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu May 07 2020
@author: Rahul
'''
class move_robot1_andgrab_shortSM(Behavior):
	'''
	detect the object in teh cnveryor and grab the object using robot1
	'''


	def __init__(self):
		super(move_robot1_andgrab_shortSM, self).__init__()
		self.name = 'move_robot1_and grab_short'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		robot1_name = 'robot1'
		joint_values_R1Home = [1.5,-1.6,0,0,0,0]
		joint_names = ['robot1_elbow_joint','robot1_shoulder_lift_joint','robot1_shoulder_pan_joint','robot1_wrist_1_joint','robot1_wrist_2_joint','robot1_wrist_3_joint']
		joint_values_R1Pregrasp = [1.57,-1.57,0,-1.5,-1.5,0]
		# x:1589 y:669, x:108 y:377
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.home1 = joint_values_R1Home
		_state_machine.userdata.joint_names = joint_names
		_state_machine.userdata.camera_output_pose = []
		_state_machine.userdata.compute_joint_pose = joint_values_R1Home
		_state_machine.userdata.joint_names_out = []
		_state_machine.userdata.preplace1 = joint_values_R1Pregrasp
		_state_machine.userdata.speed = 100

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:37 y:32
			OperatableStateMachine.add('converyor_control_start',
										SetConveyorPowerState(stop=False),
										transitions={'succeeded': 'feeder', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'speed'})

			# x:656 y:26
			OperatableStateMachine.add('move_robot_to_pre_grasp',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=robot1_name, action_topic='/move_group'),
										transitions={'reached': 'Detect_object', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'preplace1', 'joint_names': 'joint_names'})

			# x:1334 y:14
			OperatableStateMachine.add('activate gripper',
										VacuumGripperControlState(enable='true', service_name='/gripper1/control'),
										transitions={'continue': 'compute_grasp_config_2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:1571 y:21
			OperatableStateMachine.add('compute_grasp_config_2',
										ComputeGraspState(group=robot1_name, offset=0.16, joint_names=joint_names, tool_link='vacuum_gripper1_suction_cup', rotation=3.14),
										transitions={'continue': 'move_robot_to_pre_grasp_2', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'camera_output_pose', 'joint_values': 'compute_joint_pose', 'joint_names': 'joint_names_out'})

			# x:1556 y:197
			OperatableStateMachine.add('move_robot_to_pre_grasp_2',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=robot1_name, action_topic='/move_group'),
										transitions={'reached': 'move_robot1_home', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'compute_joint_pose', 'joint_names': 'joint_names_out'})

			# x:1570 y:361
			OperatableStateMachine.add('move_robot1_home',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=robot1_name, action_topic='/move_group'),
										transitions={'reached': 'activate gripper_2', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'home1', 'joint_names': 'joint_names'})

			# x:1519 y:475
			OperatableStateMachine.add('activate gripper_2',
										VacuumGripperControlState(enable='false', service_name='/gripper1/control'),
										transitions={'continue': 'finished', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:244 y:23
			OperatableStateMachine.add('feeder',
										ControlFeederState(activation=True),
										transitions={'succeeded': 'move_robot_home', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})

			# x:445 y:21
			OperatableStateMachine.add('move_robot_home',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group=robot1_name, action_topic='/move_group'),
										transitions={'reached': 'move_robot_to_pre_grasp', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'home1', 'joint_names': 'joint_names'})

			# x:855 y:26
			OperatableStateMachine.add('Detect_object',
										DetectPartCameraState(ref_frame='robot1_base_link', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'converyor_control_stop', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'camera_output_pose'})

			# x:1082 y:21
			OperatableStateMachine.add('converyor_control_stop',
										SetConveyorPowerState(stop=True),
										transitions={'succeeded': 'activate gripper', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'speed'})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
