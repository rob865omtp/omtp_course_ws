#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from omtp_factory_flexbe_states.set_conveyor_power_state import SetConveyorPowerState
from omtp_factory_flexbe_states.control_feeder_state import ControlFeederState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu May 07 2020
@author: Rahul
'''
class move_robot1_andgrabSM(Behavior):
	'''
	detect the object in teh cnveryor and grab the object using robot1
	'''


	def __init__(self):
		super(move_robot1_andgrabSM, self).__init__()
		self.name = 'move_robot1_and grab'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		robot1_name = 'robot1'
		joint_values_R1Home = [1.5,-1.6,0,0,0,0]
		joint_names = ['robot1_elbow_joint','robot1_shoulder_lift_joint','robot1_shoulder_pan_joint','robot1_wrist_1_joint','robot1_wrist_2_joint','robot1_wrist_3_joint']
		# x:873 y:535, x:108 y:377
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.home1 = joint_values_R1Home
		_state_machine.userdata.joint_names = joint_names
		_state_machine.userdata.camera_output_pose = []
		_state_machine.userdata.compute_joint_config = []
		_state_machine.userdata.compute_joint_pose = joint_values_R1Home
		_state_machine.userdata.speed = 20

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:120 y:139
			OperatableStateMachine.add('converyor',
										SetConveyorPowerState(stop='false'),
										transitions={'succeeded': 'feeder', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'speed'})

			# x:478 y:155
			OperatableStateMachine.add('feeder',
										ControlFeederState(activation='true'),
										transitions={'succeeded': 'finished', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
