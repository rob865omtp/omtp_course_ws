# Lecture 7 Assignment:
Using FlexBE to make a state machine design that use the conveyor to move objects into the logical camera for the robot1 to pick and place.  The entire solution can be seen in the gif and the state figure where it performs both conveyor movement of the object and the robot picks the object up.
![Gif showing the entire assignment](lecture7.gif)
![Figure of the states](Assignment1b.png)
