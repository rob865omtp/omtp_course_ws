# OMTP Project


## Overview
This repository is gathering of all the exercises in the course object manipulation and task planning, by Group 865 of the second semester Msc Robotics at Aalborg university.

###### (Make sure to clone the relevant branch! The master branch contains all the stable exercises and solutions, while the ROS_MELODIC branch is the working branch for new features. The master branch has the exercises without the catkin workspace (src))

## Table of contents
* [Prerequisites](#prerequisites)
* [Assignments](#assignment)
* [Usage](#usage)
* [Contributing](#contributing)
* [Authors](#authors)

## Prerequisites

`Ubuntu 18.04`, `ROS Melodic` and `Python >= 3.5` virtual environment is required.


## Assignments
# Lecture 1 & (2git) Assignment:
URDF and Xacro manipulation, in the form of moving stuff around, and changing parameters. The assignment was to modify a given environment by adding two robot arms(UR5 and AAB IRB 6640) and deleting certain bins.
## Usage

Clone the repository, and build it. The environment can be seen by running:
```bash
$ roslaunch omtp_support visualize_omtp_factory.launch
``` 

Lecture 2 introduces the concept of version control using git. The exercise here was to just create a repo and commit readme file.

![Figure of factory](Lecture_1/Assignment1.png)

# Lecture 3 & 4: (Exercises not manditory)
The aim of this exercise is to modifying the matlab code to add another fuction called JMDP and also modify other parametrs of the code.
The code is then run on a Mujoco Haptix Siftware to simulate pick and place operations.
This code has the positions of the blocks hard coded and hence the positions had to be calibrated mannually to pick the objects accurately.

In the later stages of the exercise it is required to make a Matlab function for detecting contacts based on simulated forces.

# Lecture 5 Assignment:
Learn the Moveit package by configuring the two UR5 robots using the MoveIt Setup Assistant and create some simple movements. A script is then made with the movements of the robots, which are executet using the MoveIt Commander.
The third assignment is the completion of the script to do a pick and place movement.
![Assignment 1 and 2](Lecture_5/Assignment12.png).


![UR robot Moving](Lecture_5/Assignment3.gif).

# Lecture 6 Assignment:
Set up and configure two logical cameras in the world and verify the tf tree about the correct structure.
![Figure of the tf three](Lecture_6/frames.png).
![Figure of the the workspace](Lecture_6/asgn2.png).
Implement an object and complete the lecture6_assignement3 script, which will then transform the pose of the object to the suction_cup for robot2.

In the fourth assignment a python script for the second robot has been made that pick up an object next to the robot and places it in to the bin. Our solution can be seen in the gif.
![UR robot Moving](Lecture_6/lecture6.gif).


# Lecture 7 Assignment:
Using FlexBE to make a state machine design that uses the conveyor to move objects into the logical camera FOV, which sends the object's position to the robot1 to pick and place.  The entire solution can be seen in the gif and the state figure where it performs both conveyor movement of the object and the robot picks the object up.
![Gif showing the entire assignment](Lecture_7/lecture7.gif).
![Figure of the states](Lecture_7/Assignment1b.png).

# Lecture 8 Assignment:

Use Google Colab to identify if someone has a mask on or off. The whole team has made a gif to show how it is detecting.

![Do we weare a mask? cuff cuff covid-19](Lecture_8/Lecture8.gif).

# Lecture 9 Assignment

## Overview
Lecture 9 contains the source code used and the result we got from it.

## Setup CartPole

Make a Python 3.5 or above virtual environment e.g. with Anaconda/virtualenv (example will be given with virtualenv).

1. Install OpenAI gym: `https://github.com/openai/gym`
2. Install TensorFlow + TensorBoard version 1.15 or less
3. Install Stable-Baselines: `https://stable-baselines.readthedocs.io/en/master/guide/install.html`
4. Train the agent
   - Find DQN example on stable-baselines and run the code to train the agent
5. Observe the training with TensorBoard

## Getting started

### Prerequisites

`Ubuntu 18.04`, `ROS Melodic` and `Python >= 3.5` is required.

```
pip install --upgrade pip
pip3 install virtualenv
```
In the /Lecture_9/ folder create a virtual environment named venv:
```
virtualenv venv
```
Source workspace before using virtual environment:
```
source ./venv/bin/activate
```
Now install dependencies:
```
pip3 install tensorflow==1.15.0
pip install gym
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev
pip install stable-baselines
```

## Assignment
Run the following command in the sourced virtual environment, and the model will trained and an animation will be run.
```
python3 cart_pole.py
```
To view the graph of the model with TensorBoard use the following command and open the webside `http://localhost:6006/`.
```
tensorboard --logdir logs
```

The model we trained can be seen in the graph and gif below.

![Gif showing the trained model](/Lecture_9/Cart_example.gif)
![Graph showing the trained model](/Lecture_9/graph_L9.png)





## Contributing
All the work will be pushed by the project group.

## Authors

Ditte Damgaard Albertsen:	        dalber16@student.aau.dk

Rahul Ravichandran:			rravic19@student.aau.dk

Rasmus Thomsen:				rthoms16@student.aau.dk

Mathiebhan Mahendran:			mmahen15@student.aau.dk

Alexander Staal:			astaal16@student.aau.dk

Carolina Bucle Infinito:                cgomez19@student.aau.dk
