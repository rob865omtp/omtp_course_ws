# Lecture 5 Assignment:
Learn the Moveit package by configuring the two UR5 robots using the MoveIt Setup Assistant and create some simple movements. A script is then made with the movements of the robots, which are executet using the MoveIt Commander.
The third assignment is the completion of the script to do a pick and place movement.
![Assignment 1 and 2](Assignment12.png).


![UR robot Moving](Assignment3.gif).
