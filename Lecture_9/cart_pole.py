# import gym
# import numpy as np

# from stable_baselines.common.policies import MlpPolicy
# from stable_baselines.common.vec_env import SubprocVecEnv
# from stable_baselines.common import set_global_seeds, make_vec_env
# from stable_baselines import ACKTR

# def make_env(env_id, rank, seed=0):
#     """
#     Utility function for multiprocessed env.

#     :param env_id: (str) the environment ID
#     :param num_env: (int) the number of environments you wish to have in subprocesses
#     :param seed: (int) the inital seed for RNG
#     :param rank: (int) index of the subprocess
#     """
#     def _init():
#         env = gym.make(env_id)
#         env.seed(seed + rank)
#         return env
#     set_global_seeds(seed)
#     return _init

# if __name__ == '__main__':
#     env_id = "CartPole-v1"
#     num_cpu = 4  # Number of processes to use
#     # Create the vectorized environment
#     env = SubprocVecEnv([make_env(env_id, i) for i in range(num_cpu)])

#     # Stable Baselines provides you with make_vec_env() helper
#     # which does exactly the previous steps for you:
#     # env = make_vec_env(env_id, n_envs=num_cpu, seed=0)

#     model = ACKTR(MlpPolicy, env, verbose=1)
#     model.learn(total_timesteps=25000)

#     obs = env.reset()
#     for _ in range(1000):
#         action, _states = model.predict(obs)
#         obs, rewards, dones, info = env.step(action)
#         env.render()

#############################

# import gym

# from stable_baselines.common.policies import MlpPolicy
# from stable_baselines.common.vec_env import DummyVecEnv
# from stable_baselines import PPO2

# env = gym.make('CartPole-v1')
# # Optional: PPO2 requires a vectorized environment to run
# # the env is now wrapped automatically when passing it to the constructor
# # env = DummyVecEnv([lambda: env])

# model = PPO2(MlpPolicy, env, verbose=1)
# model.learn(total_timesteps=100000)

# obs = env.reset()
# for i in range(1000):
#     action, _states = model.predict(obs)
#     obs, rewards, dones, info = env.step(action)
#     env.render()



##################################
import gym 
from stable_baselines.common.vec_env import DummyVecEnv 
from stable_baselines.deepq.policies import MlpPolicy
from stable_baselines import DQN 
# CartPole-v1
env = gym.make('CartPole-v1')
model = DQN(MlpPolicy, env, verbose=1, tensorboard_log='./logs')

 

# Set whether to train or test
train_model = False
model_filename = 'dqn_cartpole_100000'

 

# Train model
if train_model:
    model.learn(total_timesteps=65000)
    model.save(model_filename)
# Test model
else:
    # del model # remove to demonstrate saving and loading
    model = DQN.load(model_filename)

 

    # Enjoy trained agent
    obs = env.reset()
    while True:
        action, _states = model.predict(obs)
        obs, rewards, dones, info = env.step(action)
        env.render()