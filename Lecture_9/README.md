# Lecture 9 Assignment

## Overview
Lecture 9 contains the source code used and the result we got from it.

## Setup CartPole

Make a Python 3.5 or above virtual environment e.g. with Anaconda/virtualenv (example will be given with virtualenv).

1. Install OpenAI gym: `https://github.com/openai/gym`
2. Install TensorFlow + TensorBoard version 1.15 or less
3. Install Stable-Baselines: `https://stable-baselines.readthedocs.io/en/master/guide/install.html`
4. Train the agent
   - Find DQN example on stable-baselines and run the code to train the agent
5. Observe the training with TensorBoard

## Getting started

### Prerequisites

`Ubuntu 18.04`, `ROS Melodic` and `Python >= 3.5` is required.

```
pip install --upgrade pip
pip3 install virtualenv
```
In the /Lecture_9/ folder create a virtual environment named venv:
```
virtualenv venv
```
Source workspace before using virtual environment:
```
source ./venv/bin/activate
```
Now install dependencies:
```
pip3 install tensorflow==1.15.0
pip install gym
sudo apt-get update && sudo apt-get install cmake libopenmpi-dev python3-dev zlib1g-dev
pip install stable-baselines
```

## Assignment
Run the following command in the sourced virtual environment, and the model will trained and an animation will be run.
```
python3 cart_pole.py
```
To view the graph of the model with TensorBoard use the following command and open the webside `http://localhost:6006/`.
```
tensorboard --logdir logs
```

The model we trained can be seen in the graph and gif below.

![Gif showing the trained model](/Lecture_9/Cart_example.gif)
![Graph showing the trained model](/Lecture_9/graph_L9.png)
